﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;



namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Food food = new Food() { Food1 = "Soto Ayam", Food2 = "Nasi Goreng", Food3 = "Ketoprak" };
            string filePath = "data.save";
            DataSerializer dataSerializer = new DataSerializer();
            Food f = null;

            dataSerializer.BinarySerialize(food, filePath);
            f = dataSerializer.BinaryDeserialize(filePath) as Food;
            Console.WriteLine(f.Food1);
            Console.WriteLine(f.Food2);
            Console.WriteLine(f.Food3);
            Console.ReadLine();
        }
    }

    [Serializable]
    public class Food
    {
        public string Food1 { get; set; }
        public string Food2 { get; set; }
        public string Food3 { get; set; }
    }

    class DataSerializer
    {
        public void BinarySerialize(object data, string filePath)
        {
            FileStream fileStream;
            BinaryFormatter bf = new BinaryFormatter();
            if (File.Exists(filePath)) File.Delete(filePath);
            fileStream = File.Create(filePath);
            bf.Serialize(fileStream, data);
            fileStream.Close();
        }

        public object BinaryDeserialize(string filePath)
        {
            object obj = null;
            FileStream fileStream;
            BinaryFormatter bf = new BinaryFormatter();
            if (File.Exists(filePath))
            {
                fileStream = File.OpenRead(filePath);
                obj = bf.Deserialize(fileStream);
                fileStream.Close();
            }
            return obj;
        }
    }
}





